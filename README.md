# Code test


## Task description
For this task we would like you to create a small application with PHP, JS, HTML and CSS. 
Feel free to use what ever frameworks you see fit. 

It should be designed as a cool index page and contain a list of items.


For you to be able to show of your skills in PHP we would like you to create a server heavy application where the set of list items (example 100000 songs or books) are fetched/generated once and stored on the server.
Feel free to generate or fetch the list items in what ever manner you find fit. 


### Obligatory functionality
* The user should be able to edit the data of a item in the list. 

* Each list item should contain something like a title, description, authors, publishers, countries available in or whatever you see fit.

* The list should only be populated with about 50 items at a time. (To fit on the screen basically)

* The user should be able to search, filter and sort through all the items in the whole list on one of the variables in the item. 

* The application should have a responsive design.

* Short documentation on how to run the application.

### Nice to have / extra 
* Search the list while typing, for every letter put into the search box.

* Handle images for the items.

* Filter the items by multible varaibles.

* Sort the list based on all the available varaibles.

* Write tests in whatever framework you find fit.

Be creative, logical and have loads of fun with it! 
